<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailUtil extends Mailable {
    use Queueable, SerializesModels;

    public $datamessage;
    public $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datamessage, $token) {

        $this->datamessage = $datamessage;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

        if ($this->token == null) {
            return $this->from('info@IMLHC.com')->view('emails.templateMail');
        } else {
            return $this->from('info@IMLHC.com')->view('emails.templateResetPassword')->with(['token' => $this->token]);
        }
    }
}
