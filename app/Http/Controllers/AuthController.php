<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Mail\EmailUtil;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller {

	public $key;
	public $successCode;

	public function __construct(){
		$this->key = config('app.customkey');
		$this->successCode = config('app.successcode');
	}
    
    public function register(Request $req) {

    	$validator = Validator::make($req->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $verification = rand(0, 9999);

        $user = User::create([
            'name' => $req->name,
            'email' => $req->email,
            'verification_code' => $verification
        ]);

        $success['data'] = ['name' => $user->name, 'email' => $user->email];

        $datamessage = [
            'title' => 'Halo '.$user->name,
            'body'  => 'Selamat proses registrasi berhasil, <br>kode verifikasi = '.$user->verification_code
        ];

        Mail::to($user->email)->send(new EmailUtil($datamessage, null));


        return response()->json(['data' => $success, 'responsecode' => $this->successCode]);

    }

    public function verification(Request $req){

        $validator = Validator::make($req->all(), [
            'password' => 'required|min:6'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $emailuser = User::whereEmail($req->email)->first();
        if ($emailuser == null) {
            return response()->json(['message' => 'email tidak terdaftar!', 'responsecode' => 300]);
        }

        $user = User::where('verification_code',$req->verification_code)->first();
        if ($user == null) {
            return response()->json(['message' => 'harap periksa kembali kode verifikasi anda!'], 300);
        }

        $user->password = bcrypt($req->password);
        $user->save();

        return response()->json(['success' => true, 'responsecode' => $this->successCode]);
    }

    public function login(){

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken($this->key)->accessToken;
            return response()->json(['success' => $success, 'responsecode' => $this->successCode]);
        }
        else{
            return response()->json(['message'=>'Unauthorised'], 401);
        }
    }

    public function logout() {

        auth()->logout();
        return response()->json(['message' => 'User successfully signed out', 'responsecode' => $this->successCode]);
    }

    public function sendReset(Request $request) {

        if (!$this->validEmail($request->email)) {
            return response()->json(['message' => 'Email tidak terdaftar', 'responsecode' => 300]);
        } else {
            $this->sendMail($request->email);
            return response()->json(['message' => 'Harap periksa email anda', 'responsecode' => $this->successCode]);
        }
    }

    public function sendMail($email) {

        $token = $this->generateToken($email);
        Mail::to($email)->send(new EmailUtil(null, $token));
    }

    public function validEmail($email) {

        return !!User::where('email',$email)->first();
    }

    public function generateToken($email){

      $isOtherToken = DB::table('password_resets')->where('email', $email)->first();

      if($isOtherToken) {
        return $isOtherToken->token;
      }

      $token = Str::random(80);;
      $this->storeToken($token, $email);
      return $token;
    }

    public function storeToken($token, $email){
        DB::table('password_resets')->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now()            
        ]);
    }


    public function profile() {
    	// return response()-> json(['user' => auth()->user(), 'responsecode' => $this->successCode]);
        return null;
    }
}
