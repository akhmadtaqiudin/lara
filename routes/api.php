<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('verification', 'AuthController@verification')->name('verification');
Route::post('register', 'AuthController@register')->name('register');
Route::post('login', 'AuthController@login')->name('login');
Route::post('logout', 'AuthController@logout')->name('logout');

Route::middleware('auth:api')->group(function () {
    Route::post('forgot-password','AuthController@sendReset');
    Route::post('change-password', 'AuthController@passwordResetProcess');
    Route::get('user','AuthController@profile');
    // Route::resource('products','ProductController');
});
